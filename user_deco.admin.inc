<?php

/**
 * @file
 *   Administrative page callbacks for user_deco.module.
 */

//============
//IMPORT FORM.
//============

/**
 * A form to import decos.
 * @see user_deco_import_validate()
 * @see user_deco_import_submit()
 */
function user_deco_import(&$form_state) {
  $form['explanation'] = array('#value' => '<p>'. t('This form allows you to import decos.  <strong>Most of the checks normally applied when adding decos are not applied on this page, including image size and URL validity.  Be careful!</strong>  Protected directories and SSL directories may not work.') .'</p>');
  $form['format'] = array('#value' => '<p>'. t('Put each deco on a new line, and separate the title, URL, and price with a comma, like this:<br />title, url, price<br />title2, url2, price') .'</p>');
  $default = '';
  if (drupal_substr(phpversion(), 0, 1) == 5) {
    if ($_GET['dir']) {
      $default = _user_deco_create_csv($_GET['dir']);
    }
    $form['get_import'] = array(
      '#type' => 'textfield',
      '#title' => t('Get import text from directory'),
      '#description' => '<p>'. t('Enter a directory and User Deco will automatically generate the text you need to import images in that directory.  If you have text in the textarea below when you submit the form, it will still be processed.') .'</p>'.
        '<p>'. t('Files in the specified directory should have the format user_deco_title_10.ext where User Deco Title is the title, 10 is the price, and ext is a valid image extension (jpg, gif, png).') .'</p>',
    );
  }
  $form['import'] = array(
    '#type' => 'textarea',
    '#title' => t('Import decos'),
    '#description' => t('Enter decos in CSV format.  Put each deco on a separate line, and the title, URL, and price (in that order) should be separated by a comma.  Do not use pipes (|).'),
    '#default_value' => $default,
    '#rows' => 10,
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Import'));
  return $form;
}

/**
 * Validation of the import form.
 * @see user_deco_import()
 * @see user_deco_import_submit()
 */
function user_deco_import_validate($form, &$form_state) {
  if ($form_state['values']['import'] != '') {
    if (strpos($form_state['values']['import'], '|') !== FALSE) {
      form_set_error('import', t('You must not use pipe characters (|) in this form.'));
    }
    $values = preg_replace('/(\r\n?|\n)/', '|', $form_state['values']['import']);
    //This mimics _user_deco_save_deco() but it's unavoidable since we can't set form errors in a generic API function.
    $lines = explode('|', $values);
    foreach ($lines as $line) {
      $array = explode(',', $line);
      $number = count($array);
      if ($number != 3) {
        form_set_error('import', t('Each line must have three parameters: title, URL, and price, in that order.'));
        break;
      }
      $array[2] = trim($array[2]);
      if (!is_numeric($array[2]) || $array[2] != round($array[2])) {
        form_set_error('import', t('The third parameter, price, must be an integer!'));
      }
    }
  }
}

/**
 * Submit function for the import form.
 * @see user_deco_import()
 * @see user_deco_import_validate()
 */
function user_deco_import_submit($form, &$form_state) {
  $values = preg_replace('/(\r\n?|\n)/', '|', $form_state['values']['import']);
  if ($values) {
    $lines = _user_deco_parse_csv($values);
    foreach ($lines as $line) {
      $saved[] = _user_deco_save_deco($line['title'], $line['url'], '', $line['price']);
    }
    drupal_set_message(t('!count decos were saved.', array('!count' => count($saved))));
  }
  if ($form_state['values']['get_import']) {
    $form_state['redirect'] = array($_GET['q'], 'dir='. $form_state['values']['get_import']);
  }
}

//==============
//SETTINGS FORM.
//==============

/**
 * Admin settings form.
 * @see user_deco_admin_validate()
 * @see user_deco_admin_submit()
 */
function user_deco_admin(&$form_state) {
  $form['user_deco_flag'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow re-flagging'),
    '#description' => t('Allow users to flag a deco again after an administrator has cleared flags for that deco.'),
    '#default_value' => variable_get('user_deco_flag', 0),
  );
  $form['user_deco_buy'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow buying the same deco multiple times'),
    '#default_value' => variable_get('user_deco_buy', 0),
  );
  $form['user_deco_autoflag'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto-flag'),
    '#description' => t('Enabling auto-flagging and setting "number of flags before deco is hidden" to 1 will allow administrators to moderate every deco.  If you do this, you should not allow flagging for non-admin.'),
    '#default_value' => variable_get('user_deco_autoflag', 0),
  );
  $form['user_deco_give'] = array(
    '#type' => 'checkbox',
    '#title' => t('Give users decos they added'),
    '#default_value' => variable_get('user_deco_give', 1),
  );
  $form['user_deco_private'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow private decos'),
    '#description' => t('Private decos will not prompt Activity messages or show up on the main Gallery page.'),
    '#default_value' => variable_get('user_deco_private', 1),
  );
  $form['user_deco_percent'] = array(
    '#type' => 'select',
    '#title' => t('Percent of the userpoints spent to buy a deco that are returned when the deco is removed'),
    '#default_value' => variable_get('user_deco_percent', 50),
    '#options' => array(0 => '0', 25 => '25', 50 => '50', 75 => '75', 100 => '100'),
  );
  $form['user_deco_number'] = array(
    '#type' => 'select',
    '#title' => t('Number of decos to show on the gallery page'),
    '#default_value' => variable_get('user_deco_number', 16),
    '#options' => array(9 => '9', 16 => '16', 20 => '20'),
  );
  $form['user_deco_hide'] = array(
    '#type' => 'select',
    '#title' => t('Number of flags before deco is hidden'),
    '#default_value' => variable_get('user_deco_hide', 3),
    '#options' => array(0 => t('Always show'), 1 => '1', 3 => '3', 5 => '5', 10 => '10', 25 => '25'),
  );
  $form['user_deco_max_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum width for a deco'),
    '#description' => t('Use zero for no limit.'),
    '#default_value' => variable_get('user_deco_max_width', 100),
    '#size' => 4,
    '#maxlength' => 4,
    '#required' => TRUE,
  );
  $form['user_deco_max_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum height for a deco'),
    '#description' => t('Use zero for no limit.'),
    '#default_value' => variable_get('user_deco_max_height', 100),
    '#size' => 4,
    '#maxlength' => 4,
    '#required' => TRUE,
  );
  $form['user_deco_min_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum width for a deco'),
    '#description' => t("Be careful not to restrict your users too much unless they know what they're doing."),
    '#default_value' => variable_get('user_deco_min_width', 8),
    '#size' => 4,
    '#maxlength' => 4,
    '#required' => TRUE,
  );
  $form['user_deco_min_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum height for a deco'),
    '#description' => t("Be careful not to restrict your users too much unless they know what they're doing."),
    '#default_value' => variable_get('user_deco_min_height', 8),
    '#size' => 4,
    '#maxlength' => 4,
    '#required' => TRUE,
  );
  $form['user_deco_min_price'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum price for a deco'),
    '#description' => t('Set to the same as the maximum to set a fixed price.'),
    '#default_value' => variable_get('user_deco_min_price', 0),
    '#size' => 4,
    '#maxlength' => 11,
    '#required' => TRUE,
  );
  $form['user_deco_max_price'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum price for a deco'),
    '#description' => t('Set to the same as the minimum to set a fixed price or set to -1 to allow any price above the minimum.'),
    '#default_value' => variable_get('user_deco_max_price', 10),
    '#size' => 4,
    '#maxlength' => 11,
    '#required' => TRUE,
  );
  $form['user_deco_flood'] = array(
    '#type' => 'textfield',
    '#title' => t('Send Deco flood limit'),
    '#description' => t('The maximum amount of times each user is allowed to send decos per hour.  Set to zero for unlimited.'),
    '#default_value' => variable_get('user_deco_flood', 10),
    '#size' => 4,
    '#maxlength' => 11,
    '#required' => TRUE,
  );
  $form['user_deco_learn_points'] = array(
    '#type' => 'textfield',
    '#title' => t('URL where users can learn about how to get more @points', array('@points' => variable_get(USERPOINTS_TRANS_LCPOINTS, 'points'))),
    '#description' => t('Leave blank to ignore.  Can be a relative or full URL.'),
    '#default_value' => variable_get('user_deco_learn_points', ''),
  );
  $form['user_deco_categories'] = array(
    '#type' => 'textarea',
    '#title' => t('Categories'),
    '#description' => t("Decos can be tagged with one of these categories.  If you don't enter categories, users will be able to enter their own categories.") .' '.
      t("If you only enter one category, that category will be automatically assigned.  Categories are not required, but make searching decos more accurate.") .' '.
      t("Separate categories with commas: funny, foreign affairs, drupal."),
    '#default_value' => variable_get('user_deco_categories', ''),
  );
  $form['user_deco_multiple_categories'] = array(
    '#type' => 'textfield',
    '#title' => t('Allow multiple categories'),
    '#description' => t('Enter the maximum number of categories users are allowed to choose.  Enter "zero" to allow choosing all categories (or as many as fit in the textfield if you allow users to choose their own categories).  Irrelevant if you only entered one category above.'),
    '#default_value' => variable_get('user_deco_multiple_categories', 0),
  );
  if (module_exists('privatemsg')) {
    $form['user_deco_pm_user'] = array(
      '#type' => 'textfield',
      '#title' => t('Sender of Deco Received private messages'),
      '#description' => t('Leave blank to prevent private message alerts from sending when a user receives a deco.'),
      '#default_value' => variable_get('user_deco_pm_user', ''),
      '#autocomplete_path' => 'user/autocomplete',
    );
    $formats = filter_formats();
    $options = array();
    foreach ($formats as $format) {
      $options[$format->format] = check_plain($format->name);
    }
    $form['user_deco_pm_format'] = array(
      '#type' => 'select',
      '#title' => t('Format in which to send Deco Received private messages'),
      '#default_value' => variable_get('user_deco_pm_format', FILTER_FORMAT_DEFAULT),
      '#options' => $options,
    );
  }
  return system_settings_form($form);
}

/**
 * Validation of the settings form.
 * @see user_deco_admin()
 * @see user_deco_submit()
 */
function user_deco_admin_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['user_deco_max_width']) || $form_state['values']['user_deco_max_width'] <= 0) {
    form_set_error('user_deco_max_width', t('The maximum width must be a number greater than zero.'));
  }
  if (!is_numeric($form_state['values']['user_deco_max_height']) || $form_state['values']['user_deco_max_height'] <= 0) {
    form_set_error('user_deco_max_height', t('The maximum height must be a number greater than zero.'));
  }
  if (!is_numeric($form_state['values']['user_deco_min_width']) || $form_state['values']['user_deco_min_width'] < 0) {
    form_set_error('user_deco_min_width', t('The minimum width must be a number greater than or equal to zero.'));
  }
  if (!is_numeric($form_state['values']['user_deco_min_height']) || $form_state['values']['user_deco_min_height'] < 0) {
    form_set_error('user_deco_min_height', t('The minimum height must be a number greater than or equal to zero.'));
  }
  if (!is_numeric($form_state['values']['user_deco_min_price']) || $form_state['values']['user_deco_min_price'] < 0) {
    form_set_error('user_deco_min_price', t('The minimum price must be a number greater than or equal to zero.'));
  }
  if (!is_numeric($form_state['values']['user_deco_max_price']) || $form_state['values']['user_deco_max_price'] < -1) {
    form_set_error('user_deco_max_price', t('The maximum price must be a number greater than or equal to -1.'));
  }
  if ($form_state['values']['user_deco_max_price'] < $form_state['values']['user_deco_min_price'] && $form_state['values']['user_deco_max_price'] != -1) {
    form_set_error('user_deco_max_price', t('The maximum price must be greater than or equal to the minimum price, except that it may be set to -1 to allow any price.'));
  }
  if (!is_numeric($form_state['values']['user_deco_flood']) || $form_state['values']['user_deco_flood'] < 0) {
    form_set_error('user_deco_flood', t('The Send Deco flood limit must be a number greater than or equal to 0.'));
  }
  if (!is_numeric($form_state['values']['user_deco_multiple_categories']) || $form_state['values']['user_deco_multiple_categories'] < 0 || $form_state['values']['user_deco_multiple_categories'] != round($form_state['values']['user_deco_multiple_categories'])) {
    form_set_error('user_deco_multiple_categories', t('You must allow a whole number of categories greater than or equal to zero.'));
  }
  if ($form_state['values']['user_deco_pm_user']) {
    $username_exists = db_result(db_query("SELECT uid FROM {users} WHERE name = '%s'", $form_state['values']['user_deco_pm_user']));
    if (!$username_exists) {
      form_set_error('user_deco_pm_user', t('The user you specified cannot be found.'));
    }
  }
}

/**
 * Submit function for the settings form.
 * @see user_deco_admin()
 * @see user_deco_validate()
 */
function user_deco_admin_submit($form, &$form_state) {
  //Trim whitespace from categories.
  $categories_orig = explode(',', $form_state['values']['user_deco_categories']);
  foreach ($categories_orig as $category) {
    $categories[] = trim($category);
  }
  $categories_final = implode(', ', $categories);

  //Save variables.
  variable_set('user_deco_flag', $form_state['values']['user_deco_flag']);
  variable_set('user_deco_buy', $form_state['values']['user_deco_buy']);
  variable_set('user_deco_autoflag', $form_state['values']['user_deco_autoflag']);
  variable_set('user_deco_percent', $form_state['values']['user_deco_percent']);
  variable_set('user_deco_number', $form_state['values']['user_deco_number']);
  variable_set('user_deco_hide', $form_state['values']['user_deco_hide']);
  variable_set('user_deco_give', $form_state['values']['user_deco_give']);
  variable_set('user_deco_private', $form_state['values']['user_deco_private']);
  variable_set('user_deco_max_width', $form_state['values']['user_deco_max_width']);
  variable_set('user_deco_max_height', $form_state['values']['user_deco_max_height']);
  variable_set('user_deco_min_width', $form_state['values']['user_deco_min_width']);
  variable_set('user_deco_min_height', $form_state['values']['user_deco_min_height']);
  variable_set('user_deco_min_price', $form_state['values']['user_deco_min_price']);
  variable_set('user_deco_max_price', $form_state['values']['user_deco_max_price']);
  variable_set('user_deco_flood', $form_state['values']['user_deco_flood']);
  variable_set('user_deco_learn_points', $form_state['values']['user_deco_learn_points']);
  variable_set('user_deco_pm_user', $form_state['values']['user_deco_pm_user']);
  variable_set('user_deco_pm_format', $form_state['values']['user_deco_pm_format']);
  variable_set('user_deco_categories', $categories_final);
  variable_set('user_deco_multiple_categories', $form_state['values']['user_deco_multiple_categories']);
  drupal_set_message(t('The configuration settings have been saved.'));
}