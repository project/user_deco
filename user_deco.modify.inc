<?php

/**
 * @file
 *   Forms for important pages in user_deco.module.
 */

//=========
//ADD FORM.
//=========

/**
 * A form to add decos.
 * @see user_deco_add_validate()
 * @see user_deco_add_submit()
 */
function user_deco_add(&$form_state) {
  if (variable_get('user_deco_give', 1)) {
    $form['instructions'] = array('#value' => '<p>'. t('This deco will be automatically added to your profile.  You will not be charged for it.') .'</p>');
  }
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The name of the deco.'),
    '#maxlength' => 255,
    '#size' => 32,
    '#required' => TRUE,
  );
  $maxwidth = variable_get('user_deco_max_width', 100);
  $maxheight = variable_get('user_deco_max_height', 100);
  $minwidth = variable_get('user_deco_min_width', 8);
  $minheight = variable_get('user_deco_min_height', 8);
  //Technically, $description breaks with Drupal convention: l() is not supposed to go inside of t().  That's okay, however, because the text in l() here is never going to be translated.
  if ($maxwidth == $minwidth && $maxheight == $minheight) {
    $description = t('Copy-paste the URL of an image from !flickr here. The image must be exactly !maxwidthx!maxheightpx. <strong>Use the URL of the photo-viewing page.</strong>', array(
      '!flickr' => l('Flickr', 'http://www.flickr.com/'),
      '!maxwidth' => $maxwidth,
      '!maxheight' => $maxheight));
  }
  else if ($minwidth == 0 && $maxwidth == 0) {
    $description = t('Copy-paste the URL of an image from !flickr here. The image must be smaller than or equal to !widthx!heightpx. <strong>Use the URL of the photo-viewing page.</strong>', array(
      '!flickr' => l('Flickr', 'http://www.flickr.com/'),
      '!width' => $maxwidth,
      '!height' => $maxheight));
  }
  else {
    $description = t('Copy-paste the URL of an image from !flickr here. The image must be smaller than or equal to !maxwidthx!maxheightpx and greater than or equal to !minwidthx!minheightpx. <strong>Use the URL of the photo-viewing page.</strong>', array(
      '!flickr' => l('Flickr', 'http://www.flickr.com/'),
      '!maxwidth' => $maxwidth,
      '!maxheight' => $maxheight,
      '!minwidth' => $minwidth,
      '!minheight' => $minheight));
  }
  //53 is the length of a standard Flickr page URL.
  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Image URL'),
    '#description' => $description,
    '#size' => 53,
    '#required' => TRUE,
  );
  $form['categories'] = _user_deco_get_categories('add');
  if (variable_get('user_deco_min_price', 0) != variable_get('user_deco_max_price', 10)) {
    if (variable_get('user_deco_max_price', 10) != -1) {
      $description = t('The number of @points required to buy this deco.  Must be between !min and !max, inclusive.', array('@points' => variable_get(USERPOINTS_TRANS_LCPOINTS, 'points'), '!min' => variable_get('user_deco_min_price', 0), '!max' => variable_get('user_deco_max_price', 10)));
    }
    else if (variable_get('user_deco_min_price', 0) != 0) {
      $description = t('The number of @points required to buy this deco.  Must be greater than or equal to !min.', array('@points' => variable_get(USERPOINTS_TRANS_LCPOINTS, 'points'), '!min' => variable_get('user_deco_min_price', 0)));
    }
    else {
      $description = t('The number of @points required to buy this deco.', array('@points' => variable_get(USERPOINTS_TRANS_LCPOINTS, 'points')));
    }
    $form['price'] = array(
      '#type' => 'textfield',
      '#title' => t('Price'),
      '#description' => $description,
      '#maxlength' => 11,
      '#size' => 4,
      '#required' => TRUE,
    );
  }
  else {
    $form['price'] = array('#type' => 'hidden', '#value' => variable_get('user_deco_min_price', 0));
    $form['show_price'] = array('#value' => '<p>'. t('<strong>Price: </strong>!price', array('!price' => variable_get('user_deco_min_price', 0))) .'</p>');
  }
  if (variable_get('user_deco_private', 1)) {
    $form['private'] = array(
      '#type' => 'select',
      '#title' => t('Privacy'),
      '#description' => t('Private decos do not show up in the main gallery, but still appear on your profile.'),
      '#default_value' => 0,
      '#options' => array(0 => t('Public'), 1 => t('Private')),
    );
  }
  else {
    $form['private'] = array('#type' => 'hidden', '#value' => 0);
  }
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}

/**
 * Validation of the add deco form.
 * @see user_deco_add()
 * @see user_deco_add_submit()
 */
function user_deco_add_validate($form, &$form_state) {
  if (drupal_strlen($form_state['values']['title']) < 2) {
    form_set_error('title', t('The title must be at least 2 characters long!'));
  }
  if (drupal_substr($form_state['values']['url'], 0, 29) != 'http://www.flickr.com/photos/' && drupal_substr($form_state['values']['url'], 0, 25) != 'http://flickr.com/photos/') {
    form_set_error('url', t("The image must come from Flickr! Use the URL that appears in the address bar when you're viewing a photo page."));
  }
  /*
  //Commented out because we're only using Flickr.
  if (!strpos($form_state['values']['url'], 'flickr') && !strpos($form_state['values']['url'], 'imageshack') && !strpos($form_state['values']['url'], 'photobucket') && !strpos($form_state['values']['url'], 'picasa')) {
    //Technically, this breaks with Drupal convention: l() is not supposed to go inside of t().  That's okay, however, because the text in l() here is never going to be translated.
    form_set_error('url', t('Images may only come from !flickr, !imageshack, !photobucket, or !picasa.', array(
      '!flickr' => l('Flickr', 'http://www.flickr.com/'),
      '!imageshack' => l('Imageshack', 'http://www.imageshack.us/'),
      '!photobucket' => l('Photobucket', 'http://www.photobucket.com/'),
      '!picasa' => l('Picasa', 'http://picasaweb.google.com/'), )));
  }
  */
  $maxwidth = variable_get('user_deco_max_width', 100);
  $maxheight = variable_get('user_deco_max_height', 100);
  $minwidth = variable_get('user_deco_min_width', 8);
  $minheight = variable_get('user_deco_min_height', 8);
  $xml_url = 'http://flickr.com/services/oembed?url='. $form_state['values']['url'];
  $xml = file_get_contents($xml_url);
  $image_url = _user_deco_parse_xml($xml, 'url');
  if (!$image_url) {
    form_set_error('url', t('The image is either private or it cannot be found.  Only public images can be used.'));
  }
  list($width, $height) = getimagesize($image_url);
  //The image does not exist (getimagesize() returns FALSE) or the image size is invalid.
  if (!$width || !$height) {
    form_set_error('url', t('The image either does not exist or has no dimensions.'));
  }
  if (($maxwidth != 0 && $width > $maxwidth) || ($maxheight != 0 && $height > $maxheight)) {
    form_set_error('url', t('Image is too big! It is !widthx!heightpx and must be smaller than or equal to !maxwidthx!maxheightpx.', array('!width' => $width, '!height' => $height, '!maxwidth' => $maxwidth, '!maxheight' => $maxheight)));
  }
  if (($minwidth != 0 && $width < $minwidth) || ($minheight != 0 && $height < $minheight)) {
    form_set_error('url', t('Image is too small! It is !widthx!heightpx and must be larger than or equal to !minwidthx!minheightpx.', array('!width' => $width, '!height' => $height, '!minwidth' => $minwidth, '!minheight' => $minheight)));
  }
  $result = db_fetch_array(db_query("SELECT did, price FROM {user_deco_decos} WHERE title = '%s' AND url = '%s'", $form_state['values']['title'], $form_state['values']['url'] .' '. $image_url));
  $form_state['#actual_url'] = $form_state['values']['url'] .' '. $image_url;
  $already_exists = $result['did'];
  $price = $result['price'];
  if ($already_exists) {
    $pointword = format_plural($price, variable_get(USERPOINTS_TRANS_LCPOINT, 'point'), variable_get(USERPOINTS_TRANS_UCPOINTS, 'Points'));
    global $user;
    $bought = db_result(db_query("SELECT COUNT(did) FROM {user_deco_users} WHERE uid = %d AND did = %d", $user->uid, $already_exists));
    if (variable_get('user_deco_buy', 0) || (!variable_get('user_deco_buy', 0) && !$bought)) {
      $buy = l(t('Buy it'), 'user_deco/buy', array('query' => 'did='. $already_exists .'&'. drupal_get_destination()));
    }
    else {
      $buy = '<span class="user_deco bought">'. t('Already bought') .'</span>';
    }
    //To technically conform to Drupal coding standards, instead of inserting a link for !buy sometimes,
    //this line should be repeated where using a link is or is not necessary, so that <a> tags can be used instead of l().
    form_set_error('url', t('This deco already exists! !buy for !points @points.', array('!buy' => $buy, '!points' => $price, '@points' => $pointword)));
  }
  if (is_array($form_state['values']['categories'])) {
    form_set_value($form['categories'], implode(', ', $form_state['values']['categories']), $form_state);
  }
  $categories = explode(',', $form_state['values']['categories']);
  if (count($categories) > variable_get('user_deco_multiple_categories', 0) && variable_get('user_deco_multiple_categories', 0) != 0) {
    form_set_error('categories', t('You have !extra too many categories on this deco; you can only have !allowed.', array('!extra' => (count($categories) - variable_get('user_deco_multiple_categories', 0)), '!allowed' => variable_get('user_deco_multiple_categories', 0))));
  }
  if (!is_numeric($form_state['values']['price']) || round($form_state['values']['price']) != $form_state['values']['price']) {
    form_set_error('price', t('The price must be a whole number.'));
  }
  if (($form_state['values']['price'] < variable_get('user_deco_min_price', 0) || $form_state['values']['price'] > variable_get('user_deco_max_price', 10)) && variable_get('user_deco_max_price', 10) != -1) {
    form_set_error('price', t('The price must be between !min and !max, inclusive.', array('!min' => variable_get('user_deco_min_price', 0), '!max' => variable_get('user_deco_max_price', 10))));
  }
}

/**
 * Submit function for the add deco form.
 * @see user_deco_add()
 * @see user_deco_add_validate()
 */
function user_deco_add_submit($form, &$form_state) {
  global $user;
  $did = _user_deco_save_deco($form_state['values']['title'], $form_state['#actual_url'], $form_state['values']['categories'], $form_state['values']['price'], $form_state['values']['private']);
  if (variable_get('user_deco_give', 1)) {
    db_query("INSERT INTO {user_deco_users} (did, uid, time_stamp) VALUES (%d, %d, %d)", $did, $user->uid, time());
  }
  $deco = user_deco_load($did);
  if (variable_get('user_deco_autoflag', 0)) {
    user_deco_decoapi($deco, 'flag');
  }
  $points = variable_get('user_deco_userpoints', 0);
  if ($points) {
    userpoints_userpointsapi(array('uid' => $user->uid , 'points' => $points, 'display' => FALSE, 'operation' => 'user_deco add'));
    drupal_set_message(t('Your deco has been submitted, and you have earned !points @points for it.', array('!points' => $points, '@points' => variable_get(USERPOINTS_TRANS_LCPOINTS, 'points'))));
  }
  else {
    drupal_set_message(t('Your deco has been submitted.'));
  }
  if (module_exists('activity') && (!variable_get('user_deco_private', 1) || !$deco->private)) {
    user_deco_activity('activity', $user->uid, $did, 'added', time());
  }
  $form_state['redirect'] = 'user_deco';
}

//==========
//EDIT FORM.
//==========

/**
 * A form to edit decos.
 * @param $deco
 *   A deco object.
 * @see user_deco_edit_validate()
 * @see user_deco_edit_submit()
 */
function user_deco_edit(&$form_state, $deco) {
  global $user;
  if (!(($deco->author == $user->uid && user_access('edit own user_decos')) || user_access('admin user_decos'))) {
    drupal_access_denied();
    drupal_set_message(t('You do not have permission to edit this deco!'), 'error');
    return;
  }
  $form['image'] = array('#value' => theme('user_deco_image', $deco) .' ');
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The name of the deco.'),
    '#default_value' => check_plain($deco->title),
    '#maxlength' => 255,
    '#size' => 32,
    '#required' => TRUE,
  );
  $form['categories'] = _user_deco_get_categories('edit', $deco->did);
  if (variable_get('user_deco_min_price', 0) != variable_get('user_deco_max_price', 10)) {
    if (variable_get('user_deco_max_price', 10) != -1) {
      $description = t('The number of @points required to buy this deco.  Must be between !min and !max, inclusive.', array('@points' => variable_get(USERPOINTS_TRANS_LCPOINTS, 'points'), '!min' => variable_get('user_deco_min_price', 0), '!max' => variable_get('user_deco_max_price', 10)));
    }
    else {
      $description = t('The number of @points required to buy this deco.', array('@points' => variable_get(USERPOINTS_TRANS_LCPOINTS, 'points')));
    }
    $form['price'] = array(
      '#type' => 'textfield',
      '#title' => t('Price'),
      '#description' => $description,
      '#default_value' => $deco->price,
      '#maxlength' => 11,
      '#size' => 4,
      '#required' => TRUE,
    );
  }
  else {
    $form['price'] = array('#type' => 'hidden', '#value' => $deco->price);
    $form['show_price'] = array('#value' => '<p>'. t('<strong>Price: </strong>!price', array('!price' => $deco->price)) .'</p>');
  }
  if (variable_get('user_deco_private', 1)) {
    $form['private'] = array(
      '#type' => 'select',
      '#title' => t('Privacy'),
      '#description' => t('Private decos do not show up in the main gallery, but still appear on your profile.'),
      '#default_value' => $deco->private,
      '#options' => array(0 => t('Public'), 1 => t('Private')),
    );
  }
  else {
    $form['private'] = array('#type' => 'hidden', '#value' => 0);
  }
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  $form_state['#did'] = $deco->did;
  return $form;
}

/**
 * Validation of the deco edit form.
 * @see user_deco_edit()
 * @see user_deco_edit_submit()
 */
function user_deco_edit_validate($form, &$form_state) {
  if (drupal_strlen($form_state['values']['title']) < 2) {
    form_set_error('title', t('The title must be at least 2 characters long!'));
  }
  if (is_array($form_state['values']['categories'])) {
    form_set_value($form['categories'], implode(', ', $form_state['values']['categories']), $form_state);
  }
  $categories = explode(',', $form_state['values']['categories']);
  if (count($categories) > variable_get('user_deco_multiple_categories', 0) && variable_get('user_deco_multiple_categories', 0) != 0) {
    form_set_error('categories', t('You have !extra too many categories on this deco; you can only have !allowed.', array('!extra' => (count($categories) - variable_get('user_deco_multiple_categories', 0)), '!allowed' => variable_get('user_deco_multiple_categories', 0))));
  }
  if (!is_numeric($form_state['values']['price']) || round($form_state['values']['price']) != $form_state['values']['price']) {
    form_set_error('price', t('The price must be a whole number.'));
  }
  if (($form_state['values']['price'] < variable_get('user_deco_min_price', 0) || $form_state['values']['price'] > variable_get('user_deco_max_price', 10)) && variable_get('user_deco_max_price', 10) != -1) {
    form_set_error('price', t('The price must be between !min and !max, inclusive.', array('!min' => variable_get('user_deco_min_price', 0), '!max' => variable_get('user_deco_max_price', 10))));
  }
}

/**
 * Submit function for the deco edit form.
 * @see user_deco_edit()
 * @see user_deco_edit_validate()
 */
function user_deco_edit_submit($form, &$form_state) {
  global $user;
  $deco_old = user_deco_load($form_state['#did']);
  db_query("UPDATE {user_deco_decos} SET title = '%s', categories = '%s', price = %d, private = %d WHERE did = %d", $form_state['values']['title'], $form_state['values']['categories'], $form_state['values']['price'], $form_state['values']['private'], $form_state['#did']);
  drupal_set_message(t('The deco has been updated.'));
  $deco_new = user_deco_load($form_state['#did']);
  module_invoke_all('user_deco', 'deco_saved', $user, $deco_old, $deco_new);
  $form_state['redirect'] = _user_deco_get_destination('user_deco/added');
}

//==========
//SEND FORM.
//==========

/**
 * Form to send decos to other users.
 *
 * @param $deco
 *   A deco object.
 * @see user_deco_send_validate()
 * @see user_deco_send_submit()
 */
function user_deco_send(&$form_state, $deco) {
  $form['deco_display'] = array('#value' => theme('user_deco_image', $deco));
  $form['names'] = array(
    '#type' => 'textfield',
    '#title' => t('Send to'),
    '#maxlength' => 60,
    '#autocomplete_path' => 'user_deco/autocomplete',
    '#required' => TRUE,
  );
  $form['buy'] = array(
    '#type' => 'radios',
    '#title' => t('Buy or Recommend'),
    '#default_value' => 'buy',
    '#options' => array(1 => t('Buy'), 0 => t('Recommend')),
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Send'));
  $form_state['#deco'] = $deco;
  return $form;
}

/**
 * Don't allow user to send decos to themselves or non-existent users, and don't send a deco to the same user multiple times in the same try.
 * @see user_deco_send()
 * @see user_deco_send_submit()
 */
function user_deco_send_validate($form, &$form_state) {
  global $user;
  $deco = $form_state['#deco'];
  $names_orig = explode(',', $form_state['values']['names']);
  $names = array();
  foreach ($names_orig as $name) {
    $name = trim($name);
    $name_exists = db_result(db_query("SELECT uid FROM {users} WHERE name = '%s'", $name));
    //Checks to see if the name is entered multiple times.
    $already_exists = in_array($name, $names);
    $account = user_load(array('uid' => $name_exists));
    if ($name != $user->name && $name_exists && !$already_exists && user_access('buy user_decos', $account)) {
      $bought = db_result(db_query("SELECT COUNT(did) FROM {user_deco_users} WHERE uid = %d AND did = %d", $name_exists, $deco->did));
      if ($bought) {
        $already_own[] = $name;
      }
      $names[] = $name;
      $uids[] = $name_exists;
    }
    else if (!user_access('buy user_decos', $account)) {
      $denied[] = $name;
    }
  }
  if (count($denied)) {
    form_set_error('names', t('These people do not have permission to receive this deco: @people.', array('@people' => implode(', ', $denied))));
  }
  if (!variable_get('user_deco_buy', 0) && count($already_own)) {
    form_set_error('names', t('These people have already bought this deco: @people', array('@people' => implode(', ', $already_own))));
  }
  if (!count($names)) {
    form_set_error('names', t("You must enter at least one valid user's name that is not your own."));
  }
  if ($form_state['values']['buy']) {
    $cost = $deco->price * count($names);
    if (userpoints_get_current_points($user->uid) < $cost && module_exists('userpoints_no_negative')) {
      form_set_error('names', t('You do not have enough @points to buy this deco for these people!', array('@points' => variable_get(USERPOINTS_TRANS_LCPOINTS, 'points'))));
    }
  }
  if (!flood_is_allowed('user_deco send', variable_get('user_deco_flood', 10))) {
    form_set_error('', t('You have sent decos too many times in the last hour.  Please wait and then try again.'));
  }
  $final = implode(', ', $names);
  form_set_value($form['names'], $final, $form_state);
  $form_state['#uids'] = $uids;
}

/**
 * Sends decos to other users.
 * @see user_deco_send()
 * @see user_deco_send_validate()
 */
function user_deco_send_submit($form, &$form_state) {
  global $user;
  $deco = $form_state['#deco'];
  foreach ($form_state['#uids'] as $uid) {
    db_query("INSERT INTO {user_deco_send} (sender, recipient, did, bought, time_stamp, unread) VALUES (%d, %d, %d, %d, %d, %d)", $user->uid, $uid, $deco->did, $form_state['values']['buy'], time(), 1);
    //If the Trigger module is installed, users can choose to create a trigger to send a private message or normal email as they choose.
    if (!module_exists('trigger')) {
      user_deco_send_alert(array('sender' => $user->uid, 'recipient' => $uid, 'did' => $deco->did, 'bought' => $form_state['values']['buy']));
    }
    if ($form_state['values']['buy']) {
      userpoints_userpointsapi(array('uid' => $user->uid, 'points' => (0 - $deco->price), 'display' => FALSE, 'operation' => 'user_deco send'));
      db_query("INSERT INTO {user_deco_users} (did, uid, time_stamp) VALUES (%d, %d, %d)", $deco->did, $uid, time());
    }
  }
  if (module_exists('activity') && (!variable_get('user_deco_private', 1) || !$deco->private)) {
    user_deco_activity('activity', $uid, $deco->did, 'sent', time());
  }
  drupal_set_message(t('The deco has been sent.'));
  flood_register_event('user_deco send');
  module_invoke_all('user_deco', 'deco_sent', $user, $form_state['#uids'], $deco);
  $form_state['redirect'] = _user_deco_get_destination('user_deco');
}