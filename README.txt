
=============
== Summary ==
=============

This module allows users to add images ("decos") to their profiles.  Images are
imported from popular image hosting services.  Users with the correct permission
levels can add decos to the system, buy them for their profile with Userpoints,
flag them as inappropriate, edit them, send them to others, or moderate them.
Administrators can also mass import decos with relaxed settings.

One block is provided.  On user pages, it displays the relevant user's decos. On
other pages, it displays the current user's decos.  Decos also show up on users'
profiles.

Decos are not nodes, and never will be, and so are not indexed.  However, they
are searchable from the gallery page.  When decos are added they can be put into
categories for more accurate searching.  Admin can choose to preset categories
or allow users to choose their own.

This module integrates with activity.module and depends on userpoints.module.
It also integrates with trigger.module.

==============
== Settings ==
==============

Most of User Deco's settings should be self-explanatory.  However, a common
request is the ability for administrators to moderate all decos before they show
up to the public.

To do this, admin should enable auto-flagging on the settings page, and set the
number of flags before decos are hidden to one.  If you do this, you will
probably not want to give the "flag user_decos" permission to non-admin.

=====================
== Development/API ==
=====================

This module's code is heavily documented and generally styled in the Drupal Way,
for any developers that would like to take a look.

For anyone who doesn't want to delve that deeply, this module provides a robust
API that you can use to take advantage of its functionality from anywhere you 
can use PHP.  For more information, see the function descriptions in the module
file itself.

==Theme Functions==
theme('user_deco_arrange', $uid)
  Displays a list of decos belonging to the relevant user.
theme('user_deco_gallery', $type='gallery', $option='recpop')
  Displays a list of decos with relevant links. $type can be one of 'gallery,'
  'mine,' 'added,' 'flagged,' and 'custom.' It determines the style of the
  result. $option is only relevant with 'gallery' and 'custom' types.  With
  'gallery' it can be 'recency,' 'popularity,' or 'recpop' (recent popularity)
  and determines the order of the results. With 'custom' it is an array of deco
  objects like user_deco_get_decos returns.
theme('user_deco_unit', $deco, $type='gallery', $instance = 1)
  Displays a deco with relevant links. $deco is a deco object and $type is the
  same as above.  If the gallery type is 'received,' $instance is the number of
  times the deco has already been rendered, so that we can show the correct
  sender.
theme('user_deco_image', $deco, $show_user_count=FALSE)
  Displays the image for a deco. $deco is a deco object. $show_user_count is a
  boolean that determines whether to show the number of users using the deco in
  the title attribute.

==Module Functions==
user_deco_get_decos($uid=0, $option='recpop', $hide_private = TRUE)
  Returns an array of deco objects. If $uid is zero all decos are returned and
  $option specifies how they should be ordered ('popularity,' 'recency,' 
  'recpop' [recent popularity] or 'flag_count'). Otherwise the returned decos
  belong to the specified user and $option determines whether to return decos
  that were bought or added by the relevant user ('bought' or 'added'). If $uid
  is zero and $option is not 'flag_count,' $hide_private controls whether only
  public decos or all decos are returned.
user_deco_load($did)
  Returns a deco object containing the did, price, url, title, author, private
  status, and flag_count. $did is the deco ID.
user_count_decos($uid=0, $distinct=FALSE)
  Returns the number of decos a user has. $distinct is a boolean: if TRUE, each
  deco counts only once, even if the user has bought it multiple times.  If
  FALSE, all decos are counted, including duplicates.
user_deco_decoapi($deco, $operation, $uid=0)
  Allows arbitrary actions to be performed on (existing) decos. $deco is a deco
  object or a deco ID. $operation can be one of 'buy,' 'clear,' 'delete,'
  'flag,' and 'remove.' $uid is the user who performed the action but is
  irrelevant for 'clear' and 'delete' operations. Returns the URL to which the
  user would be most likely to be redirected after the operation, or FALSE if an
  incorrect operation is specified.

==Hooks==
hook_user_deco_links_before($deco, $type)
  Allows adding links or text under decos when they are displayed using
  theme('user_deco_unit') as in galleries. These links are displayed before
  other links. Modules implementing this hook should return an array of strings
  (links) with numeric keys.
hook_user_deco_links_after($deco, $type)
  Works the same as hook_user_deco_links_before except that the links appear
  after other links.

==Helper Functions==
User Deco uses several "helper" functions that are not considered part of the
API, but might still be useful to you.  If you think you might need them, take a
look at the module file; functions are grouped in labeled sections, so they
should be easy to find.  Only advanced developers should use these functions!
They can be dangerous, and usually the API provides a better way to do what you
want.

==================
== Installation ==
==================

  1. Install this module as usual (FTP the files to sites/all/modules, enable 
    at admin/build/modules).  See http://drupal.org/node/176044 for help.

  2. Go to admin/settings/user_deco and adjust the settings as needed.

  3. Go to admin/user/access#module-user_deco and set the permissions there.

  4. Go to admin/build/block and put the block in the regions you want them.

  5. Still on admin/build/block, configure the settings for the block.

  6. Go to admin/build/menu and add any of the user_deco pages to your menus.

  7. Go to admin/settings/userpoints and adjust the amount of userpoints you
    want users to receive for adding decos.

  8. If you are using the Activity module, go to admin/settings/activity and
    adjust the settings for User Deco there as you wish.  If you do not save
    the page, activity integration will not work at all, even if you did not
    change any settings.


===========
== Links ==
===========

Visit the module page for more information.

Module Page: http://drupal.org/project/user_deco
Userpoints: http://drupal.org/project/userpoints
Activity: http://drupal.org/project/activity

Enable Module: http://example.com/?q=admin/build/modules
Settings Page: http://example.com/?q=admin/settings/user_deco
Set Permissions: http://example.com?q=admin/user/access#module-user_deco
Enable Block: http://example.com/?q=admin/build/block
Menu Page: http://example.com?q=admin/build/menu
Userpoints Settings: http://example.com?q=admin/settings/userpoints
Activity Settings: http://example.com?q=admin/settings/activity

User Deco Gallery: http://example.com?q=user_deco