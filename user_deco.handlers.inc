<?php

/**
 * @file
 *   Handler and confirm pages for user_deco.module.
 */

/**
 * Handles going offsite to Flickr.
 */
function user_deco_leaving(&$form_state) {
  if ($_GET['previous'] && !$_GET['next']) {
    drupal_set_message(t('No destination URL was specified.'), 'error');
    drupal_goto($_GET['previous']);
    return;
  }
  else if (!$_GET['previous'] && $_GET['next']) {
    drupal_goto($_GET['next']);
    return;
  }
  else if (!$_GET['previous'] && !$_GET['next']) {
    drupal_set_message(t('No "previous" or "next" URL was specified.'), 'error');
    drupal_goto('user_deco');
    return;
  }
  $form['info'] = array('#value' => '<p>'. t('You are about to leave this site to view the deco you clicked on Flickr.') .'</p>');
  $form['submit'] = array('#type' => 'submit', '#value' => t('Continue'), '#submit' => array('user_deco_leaving_continue'));
  $form['cancel'] = array('#type' => 'submit', '#value' => t('Cancel'), '#submit' => array('user_deco_leaving_cancel'));
  return $form;
}

/**
 * Continue offsite.
 */
function user_deco_leaving_continue($form, &$form_state) {
  global $user;
  watchdog('user_deco', '%user went offsite to view a deco at %destination', array('%user' => $user->name, '%destination' => $_GET['next']), WATCHDOG_NOTICE, $_GET['previous']);
  $form_state['redirect'] = $_GET['next'];
}

/**
 * Cancel leaving.
 */
function user_deco_leaving_cancel($form, &$form_state) {
  $form_state['redirect'] = $_GET['previous'];
}

/**
 * Handles flagging.
 */
function user_deco_flag() {
  if (!$_GET['did'] || !is_numeric($_GET['did'])) {
    drupal_set_message(t('You did not specify a deco to flag.'), 'error');
    $dest = _user_deco_get_destination('user_deco');
    drupal_goto($dest);
    return;
  }
  global $user;
  $redirect = user_deco_decoapi($_GET['did'], 'flag', $user->uid);
  $dest = _user_deco_get_destination($redirect);
  drupal_goto($dest);
}

/**
 * Adds a deco to the user's profile.
 */
function user_deco_buy() {
  if (!$_GET['did'] || !is_numeric($_GET['did'])) {
    drupal_set_message(t('You did not specify a deco to buy.'), 'error');
    $dest = _user_deco_get_destination('user_deco');
    drupal_goto($dest);
    return;
  }
  global $user;
  $deco = user_deco_load($_GET['did']);
  if (!_user_deco_has_points($deco, $user->uid)) {
    drupal_set_message(t('You do not have enough @points to buy this deco!', array('@points' => variable_get(USERPOINTS_TRANS_LCPOINTS, 'points'))), 'error');
    $dest = _user_deco_get_destination('user_deco');
    drupal_goto($dest);
    return;
  }
  $bought = db_result(db_query("SELECT COUNT(did) FROM {user_deco_users} WHERE uid = %d AND did = %d", $user->uid, $_GET['did']));
  if (variable_get('user_deco_buy', 0) || (!variable_get('user_deco_buy', 0) && !$bought)) {
    $redirect = user_deco_decoapi($_GET['did'], 'buy', $user->uid);
  }
  else {
    $redirect = 'user_deco';
    drupal_set_message(t('Sorry, you have already purchased this deco and you cannot buy it again.'));
  }
  $dest = _user_deco_get_destination($redirect);
  drupal_goto($dest);
}

/**
 * Confirms that the user wants to remove the deco from their profile.
 */
function user_deco_remove(&$form_state) {
  if ($_GET['did'] && is_numeric($_GET['did'])) {
    global $user;
    $has_deco = db_result(db_query("SELECT COUNT(time_stamp) FROM {user_deco_users} WHERE did = %d AND uid = %d", $_GET['did'], $user->uid));
    if ($has_deco) {
      $form['text'] = array('#value' => '<p>'. t('Are you sure you want to remove this deco from your profile?') .'</p>');
      $form['confirm'] = array('#type' => 'submit', '#value' => t('Continue'), '#submit' => array('user_deco_remove_continue'));
      $form['cancel'] = array('#type' => 'submit', '#value' => t('Cancel'), '#submit' => array('user_deco_remove_cancel'));
      return $form;
    }
    else {
      drupal_set_message(t('You do not have this deco, and thus cannot remove it.'), 'error');
      $dest = _user_deco_get_destination('user_deco/mine');
      drupal_goto($dest);
    }
  }
  else {
    drupal_set_message(t('You did not specify a deco to remove.'), 'error');
    $dest = _user_deco_get_destination('user_deco/mine');
    drupal_goto($dest);
  }
}

/**
 * Removes a deco from the user's profile.
 */
function user_deco_remove_continue($form, &$form_state) {
  global $user;
  $redirect = user_deco_decoapi($_GET['did'], 'remove', $user->uid);
  $form_state['redirect'] = $redirect;
}

/**
 * Cancels removing a deco.
 */
function user_deco_remove_cancel($form, &$form_state) {
  $form_state['redirect'] = _user_deco_get_destination('user_deco/mine');
}

/**
 * Confirms that the user wants to delete the deco.
 */
function user_deco_delete(&$form_state) {
  global $user;
  if ($_GET['did'] && is_numeric($_GET['did'])) {
    $exists = db_result(db_query("SELECT COUNT(title) FROM {user_deco_decos} WHERE did = %d", $_GET['did']));
    $author = db_result(db_query("SELECT auid FROM {user_deco_decos} WHERE did = %d", $_GET['did']));
    if ($exists && ($author == $user->uid || user_access('admin user_decos'))) {
      $form['text'] = array('#value' => '<p>'. t('Are you sure you want to delete this deco?') .'</p>');
      $form['confirm'] = array('#type' => 'submit', '#value' => t('Continue'), '#submit' => array('user_deco_delete_continue'));
      $form['cancel'] = array('#type' => 'submit', '#value' => t('Cancel'), '#submit' => array('user_deco_delete_cancel'));
      if ($author == $user->uid) {
        $form_state['#is_author'] = TRUE;
      }
      else {
        $form_state['#is_author'] = FALSE;
      }
      return $form;
    }
    else {
      drupal_set_message(t('Either this deco does not exist or you do not have permission to delete it.'), 'error');
      if ($author == $user->uid) {
        $dest = 'user_deco/added';
      }
      else {
        $dest = 'user_deco/flagged';
      }
      $dest = _user_deco_get_destination($dest);
      drupal_goto($dest);
    }
  }
  else {
    drupal_set_message(t('You did not specify a deco to remove.'), 'error');
    $dest = _user_deco_get_destination('user_deco/added');
    drupal_goto($dest);
  }
}

/**
 * Deletes a deco.
 */
function user_deco_delete_continue($form, &$form_state) {
  global $user;
  $redirect = user_deco_decoapi($_GET['did'], 'delete');
  $form_state['redirect'] = $redirect;
}

/**
 * Cancels deco deletion.
 */
function user_deco_delete_cancel($form, &$form_state) {
  if ($form_state['#is_author'] = TRUE) {
    $form_state['redirect'] = _user_deco_get_destination('user_deco/added');
  }
  else {
    $form_state['redirect'] = _user_deco_get_destination('user_deco/flagged');
  }
}

/**
 * Remove flags from a deco.
 */
function user_deco_clear() {
  if (!$_GET['did'] || !is_numeric($_GET['did'])) {
    drupal_set_message(t('You did not specify a deco for which to clear flags.'), 'error');
    $dest = _user_deco_get_destination('user_deco/flagged');
    drupal_goto($dest);
    return;
  }
  $redirect = user_deco_decoapi($_GET['did'], 'clear');
  $dest = _user_deco_get_destination($redirect);
  drupal_goto($dest);
}

/**
 * Ignores a deco that has been recommended to a user.
 * It does not matter whether the deco has already been ignored.
 */
function user_deco_ignore() {
  $dest = _user_deco_get_destination('user_deco/received');
  if (!$_GET['sid'] || !is_numeric($_GET['sid'])) {
    drupal_set_message(t('You did not specify a deco to ignore.'), 'error');
    drupal_goto($dest);
    return;
  }
  $result = db_fetch_array(db_query("SELECT recipient FROM {user_deco_send} WHERE sid = %d", $_GET['sid']));
  $recipient = $result['recipient'];
  global $user;
  if ($user->uid != $recipient) {
    drupal_set_message(t('You do not have permission to ignore that transaction.'), 'error');
    drupal_goto($dest);
    return;
  }
  db_query("UPDATE {user_deco_send} SET bought = -1 WHERE sid = %d", $_GET['sid']);
  drupal_set_message(t('The deco has been ignored.'));
  module_invoke_all('user_deco', 'deco_ignored', $user, $_GET['sid']);
  drupal_goto($dest);
  return;
}